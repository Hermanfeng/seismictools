using Lusa.UI.WorkBenchContract.Controls.Pane;

namespace CMSSpaceTree
{
    public class CMSSpaceTreePanViewProvider : IPaneViewDescriptorProvider
    {
        PaneViewDescriptor IPaneViewDescriptorProvider.Pane
        {
            get
            {
                return new PaneViewDescriptor(typeof(PaneViewUserControl)) { Header = "CMSSpaceTree", IsDocument = true };
            }
        }
    }
}
