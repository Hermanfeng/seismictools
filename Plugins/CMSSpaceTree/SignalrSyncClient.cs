using CMSTools;
using CMSTools.CMSSpaceTree;
using CMSTools.ShotGuidToOriginal;
using Lusa.UI.Msic.FileService;
using Lusa.UI.Msic.MessageService;
using Lusa.UI.Msic.MessageService.MessageObject;
using Lusa.UI.WorkBenchContract.Controls.Pane;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Seismic.Foreshock.Resource;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CMSSpaceTree
{
    public class SignalrSyncClient
    {
        HubConnection connection;
        bool isStarted = true;
        public static SignalrSyncClient Client { get; } = new SignalrSyncClient();
        private SyncFileEnv syncEnv;
        public void Start()
        {
            syncEnv = CMSTools.CMSSpaceTree.CMSSpaceTree.Instance.GetSyncFileEnv();
            if (!isStarted)
            {
                var syncEnv = CMSTools.CMSSpaceTree.CMSSpaceTree.Instance.GetSyncFileEnv();
                var signalRUrl = syncEnv.CmsEndpoit+"/api/cms";
                var hubname = "eventstest";
                connection = new HubConnectionBuilder()
                    .ConfigureLogging(loggingBuilder =>
                    {
                        loggingBuilder.Services.TryAddEnumerable(ServiceDescriptor.Singleton<ILoggerProvider, SeimiscLoggerProvider>());
                    })
                    .WithUrl($"{signalRUrl}/{hubname}", ops=>
                    {
                        ops.AccessTokenProvider = GetToken;
                        ops.Headers.Add("SpaceId", IdConvert.ConvertToShortGuidString(syncEnv.SpaceId));
                    })
                    .Build();

                connection.Closed += async (error) =>
                {
                    await Task.Delay(new Random().Next(0, 5) * 1000);
                    await connection.StartAsync();
                };

                connection.On<NodeEventMessage>("Receive", OnReceive);

                connection.StartAsync();
                isStarted = true;
            }
        }

        private Task<string> GetToken()
        {
            var str = @"eyJhbGciOiJSUzI1NiIsImtpZCI6IkI1NjdBNDM0ODBCM0I5MEI5OEE1NEU3Q0FEMTA3RjZCODU5QjIyQTIiLCJ0eXAiOiJKV1QiLCJ4NXQiOiJ0V2VrTklDenVRdVlwVTU4clJCX2E0V2JJcUkifQ.eyJuYmYiOjE1NTQyODkwMTEsImV4cCI6MTU1NDMxMDYxMSwiaXNzIjoiaHR0cHM6Ly9hdXRoLXFhMDEtZWFzdGFzaWEuc2Vpc21pYy1kZXYuY29tL3BsYXRmb3JtIiwiYXVkIjpbImh0dHBzOi8vYXV0aC1xYTAxLWVhc3Rhc2lhLnNlaXNtaWMtZGV2LmNvbS9wbGF0Zm9ybS9yZXNvdXJjZXMiLCJjbXMiLCJwbXMiXSwiY2xpZW50X2lkIjoiMzIxYzNkMzctMmJhZS00MzYzLWI2MmEtM2UzOTcxOWI5MzA2IiwicGxhdGZvcm0iOiJTZWlzbWljIiwianRpIjoiMWRjYTM3YTFiNjk2OTUyZWQ0MmQxNjI4YzI0ZTczZjQiLCJzY29wZSI6WyJjbXNfYXBpIiwicG1zX3JlYWQiXX0.b_ijPKJcahV8N01tZDTEivung_lLeu-R0X4Bon2Hl97AaKgoKiU0uJjxwV0w9stRynEsgXzU3ad2L5RdaQ-luQdo0zioY1rg9JqI4zJ7zYeC9r6GL2ipBKdz53ojozMHuLsNgWwF1becYv1y7LGmBzccP2ppl6FXLZ_FNsLDuCZEYFIQeSc1ZqiQ9YkJf-KZB1I5CLSA8u8AU9HlZljvyWDw9E4y_8NbDqRT-7wUPYMNybjhjmWHvQHDRrr-LFozSkF_aG3IMdimSGSpppIVEqgHY1_KDa_wIEh4mQH0zCc8nuDd69fyWisnFhxAWUJoCml620NtYiMdXL4jO_Wr4Q";
            return Task.FromResult(str);
        }

        public void OnReceive(NodeEventMessage syncInfo)
        {
            if (syncInfo.Tenant != syncEnv.Tenant || syncInfo.TraitActionName == "getChildren")
            {
                return;
            }

            if(string.IsNullOrEmpty(syncInfo.TraitActionName))
            {
                MessageService.Instance.SendMessage($"Cms signalr message [{syncInfo.Type}:{syncInfo.Tenant}:{syncInfo.TimeStamp}] received, orignal node id is {syncInfo.NodeId.ToOrignalId()}", MessageType.WARNING);
            }
           else
            {
                MessageService.Instance.SendMessage($"Cms signalr message [{syncInfo.Type}:{syncInfo.Tenant}:{syncInfo.TimeStamp}] received, triat action is {syncInfo.TraitActionName}, orignal node id is {syncInfo.NodeId.ToOrignalId()}", MessageType.WARNING);
            }

            
            if (syncInfo.Type == "NodeCreated" && syncInfo.SpaceId == syncEnv.SpaceId && syncInfo.Tenant == syncEnv.Tenant)
            {
                try
                {
                    MessageService.Instance.SendMessage($"Receive message form signalr, spaceid:{syncInfo.SpaceId}, nodeId : {syncInfo.NodeId}", customColor: System.Windows.Media.Color.FromRgb(0, 0, 255));
                    var config = CreateSyncConfiguration(syncInfo, syncEnv).GetAwaiter().GetResult();
                    var path = config.SpaceFoldrMaps[0].FolderPath;
                    MessageService.Instance.SendHyperLinkMessage($"Start to sync file with cms, file system path {path}", path);
                    var temptoken = GetDeamonServiceToken(syncInfo, syncEnv).GetAwaiter().GetResult();
                    MessageService.Instance.SendMessage($"Sync token : {temptoken.AccessToken}");
                    SetDeamonServiceToken(syncEnv.SyncDeamonEndPoint, temptoken).GetAwaiter().GetResult();
                    StartSyncFileSystem(syncInfo, syncEnv).GetAwaiter().GetResult();
                }
                catch(Exception ex)
                {
                    MessageService.Instance.SendMessage(ex);
                }
            }
        }

        private async Task StartSyncFileSystem(NodeEventMessage syncInfo, SyncFileEnv env)
        {
            var syncEndpoit = env.SyncDeamonEndPoint;
            var tenant = syncInfo.Tenant;
            var spaceId = syncInfo.SpaceId;
            var url = $"/api/syncDaemon/v1/sync/start";
            var httpClient = new HttpClient() { BaseAddress = new Uri(syncEndpoit) };
            var response = await httpClient.PostAsync(url, null);
            if (!response.IsSuccessStatusCode && response.Content != null)
            {
                var error = await response.Content.ReadAsStringAsync();
                MessageService.Instance.SendMessage(new Exception(error));
            }
        }

        private async Task<SyncConfiguration> CreateSyncConfiguration(NodeEventMessage syncInfo, SyncFileEnv env)
        {
            var syncEndpoit = env.SyncDeamonEndPoint;
            var tenant = syncInfo.Tenant;
            var spaceId = syncInfo.SpaceId;
            var folderPath = FileService.Instance.EnsureAbsoluteDirectory($"SyncFolder\\{spaceId}");
            var url = $"/api/syncDaemon/v1/configurations";
            var httpClient = new HttpClient() { BaseAddress = new Uri(syncEndpoit) };
            var syncConfig = new SyncConfiguration()
            {
                CMSEndpoint = env.CmsEndpoit,
                Tenant = tenant,
                UserName = env.UserName,
                LibraryHost = env.LibraryHost,
                SpaceFoldrMaps = new SpaceConfiguration[]
                {
                    new SpaceConfiguration()
                    {
                        SpaceId = spaceId,
                        Direction =  SyncDirection.Bidirectional,
                        FolderPath = folderPath
                    }
                }
            };
            var json = JsonConvert.SerializeObject(syncConfig);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            await httpClient.PostAsync(url, content);
            return syncConfig;
        }

        private async Task<SyncTokenObject> GetDeamonServiceToken(NodeEventMessage syncInfo, SyncFileEnv env)
        {
            var tenant = syncInfo.Tenant;
            var userName = env.UserName;
            var authServerUrl = await GetAuthServerUrl(syncInfo,env);
            var url = $"/api/syncDaemon/v1/sync/start";
            var token = CmsClientHelper.TokenMapInstance.GetToken(authServerUrl + userName, tenant);
            return new SyncTokenObject { AccessToken = token, ExpiredIn = int.MaxValue };
        }

        private async Task<string> GetAuthServerUrl(NodeEventMessage syncInfo, SyncFileEnv env)
        {
            var tenant = syncInfo.Tenant;
            var libraryUrl = $"https://{tenant}.{env.LibraryHost}/.well-known/authentication";
            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(libraryUrl);
            var result = await response.Content.ReadAsAsync<dynamic>();
            var url = (string)result.BaseUrl.ToString();
            //return url.Substring(0, url.Length - 10 - tenant.Length);
            return url;
        }


        private async Task SetDeamonServiceToken(string deamonEndPoint, SyncTokenObject token)
        {
            var syncEndpoit = deamonEndPoint;
            var url = $"/api/syncDaemon/v1/auth/token";
            var httpClient = new HttpClient() { BaseAddress = new Uri(syncEndpoit) };
            var json = JsonConvert.SerializeObject(token);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await httpClient.PostAsync(url, content);
            if (!response.IsSuccessStatusCode && response.Content != null)
            {
                var error = await response.Content.ReadAsStringAsync();
                MessageService.Instance.SendMessage(new Exception(error));
            }
        }

        public class SeimiscLoggerProvider : ILoggerProvider, ILogger
        {
            public IDisposable BeginScope<TState>(TState state)
            {
                return this;
            }

            public ILogger CreateLogger(string categoryName)
            {
                return this;
            }

            public void Dispose()
            {

            }

            public bool IsEnabled(LogLevel logLevel)
            {
                return true;
            }

            public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
            {
                if (exception != null)
                {
                    MessageService.Instance.SendMessage(exception);
                }
                else
                {
                    MessageService.Instance.SendMessage(formatter(state, exception));
                }
            }
        }

        public class SharedContent
        {
            public string SpaceId { get; set; }
            public string NodeId { get; set; }
        }

        public abstract class SpaceLevelEventMessage
        {
            [JsonProperty("category")]
            public string Category { get; protected set; }

            [JsonProperty("timeStamp")]
            [JsonConverter(typeof(UnixDateTimeConverter))]
            public DateTime TimeStamp { get; }

            [JsonProperty("type")]
            public string Type { get; }

            [JsonProperty("spaceId")]
            public string SpaceId { get; }

            [JsonProperty("tenant")]
            public string Tenant { get; }

            protected SpaceLevelEventMessage(DateTime timeStamp, string type, string spaceId, string tenant)
            {
                this.TimeStamp = timeStamp;
                this.Type = type;
                this.SpaceId = spaceId;
                this.Tenant = tenant;
            }
        }

        public class NodeEventMessage : SpaceLevelEventMessage
        {
            [JsonProperty("nodeId")]
            public string NodeId { get; set; }

            [JsonProperty("versionNumber")]
            public string VersionNumber { get; set; }

            [JsonProperty("traitName")]
            public string TraitName { get; set; }

            [JsonProperty("traitActionName")]
            public string TraitActionName { get; set; }

            [JsonProperty("traitActionParameter")]
            public string TraitActionParameter { get; set; }

            public NodeEventMessage(DateTime timeStamp, string type, string spaceId, string tenant, string nodeId, string versionNumber, string traitName, string traitActionName, string traitActionParameter)
                : base(timeStamp, type, spaceId, tenant)
            {
                this.Category = "node";
                this.NodeId = nodeId;
                this.VersionNumber = versionNumber;
                this.TraitName = traitName;
                this.TraitActionName = traitActionName;
                this.TraitActionParameter = traitActionParameter;
            }
        }

        public class SpaceEventMessage : SpaceLevelEventMessage
        {
            public SpaceEventMessage(DateTime timeStamp, string type, string spaceId, string tenant) : base(timeStamp, type, spaceId, tenant)
            {
                Category = "space";
            }
        }
    }


}
