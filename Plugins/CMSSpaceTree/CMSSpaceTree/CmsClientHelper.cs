﻿using System;
using System.Threading.Tasks;
using CMSTools.ShotGuidToOriginal;
using Seismic.ContentManagement.Client;
using Seismic.ContentManagement.Client.Builders;
using Seismic.ContentManagement.Client.Model;
using Seismic.ContentManagement.Model;
using Seismic.Foreshock.Resource;

namespace CMSTools
{
    public class CmsClientHelper
    {
        private readonly string serviceEndpoint;
        private readonly string tenant;
        internal static TokenMap TokenMapInstance = new TokenMap();
        public CmsClientHelper(string serviceEndpoint,string tenant)
        {
            this.serviceEndpoint = serviceEndpoint;
            this.tenant = tenant;
        }

        private ContentManagementClient CreateClient(string serviceEndpoint)
        {
            var token = TokenMapInstance.GetToken(serviceEndpoint, tenant);
            return new ContentManagementClient(new CustomTokenClient(token), serviceEndpoint);
        }

        public async Task<NodeResource> CreateVersionedFileAsync(string tenantId, string spaceId, string parentId, string name = null, ContentTrait contentTrait = null, string nodeId = null, VersionTrait versionTrait = null)
        {
            contentTrait = contentTrait ?? GetTenantTestBlobId(tenant);
            nodeId = nodeId ?? ShortGuid.Create().ToString();
            var versionId = ShortGuid.Create().ToString();
            versionTrait = versionTrait ?? new VersionTrait(versionId, 0, 1);
            name = name ?? nodeId.ToOrignalId();

            NodeResource node = NodeBuilder.CreateVersionedFileNode(tenantId, spaceId, parentId, name, contentTrait, nodeId, versionTrait, DateTime.UtcNow, DateTime.UtcNow);

            using (var client = CreateClient(serviceEndpoint))
            {
                var result = await client.CreateNodeAsync(tenantId, spaceId, node);
                return result.Resource;
            }
        }

        private ContentTrait GetTenantTestBlobId(string tenant)
        {
            var guid = Guid.NewGuid().ToString();

            var contentTrait = new ContentTrait()
            {
                Category = "toolTestCategory",
                ContentId = guid,
                ContentType = "toolTestType",
                Extention = "toolext",
                Size = 1000,
            };

            if (tenant == "dev")
            {
                contentTrait.ContentId = "78de6ea4-614e-4429-b406-f2b0e91bcd78";
                contentTrait.Size = 150751;
            }
            else if (tenant == "eric")
            {
                contentTrait.ContentId = "Upload-0d5cbb3c-9584-43d1-a5c5-b1666fb6f1ca";
                contentTrait.Size = 5454;
            }
            else if (tenant == "foreshockdev")
            {
                contentTrait.ContentId = "dcfeef8f-49c9-4774-8c50-faeb433aa9a4";
                contentTrait.Size = 191940;
                contentTrait.Extention = ".jpg";
            }

            return contentTrait;
        }

        public async Task<NodeResource> CreateFolderAsync(string tenantId, string spaceId, string parentId, string name = null, string nodeId = null)
        {
            var guid = Guid.NewGuid().ToString();
            nodeId = nodeId ?? guid.ToShortguid();
            name = name ?? guid;
            NodeResource node = NodeBuilder.CreateFolderNode(tenantId, spaceId, parentId, name, nodeId);
            using (var client = CreateClient(serviceEndpoint))
            {
                var result = await client.CreateNodeAsync(tenantId, spaceId, node);
                return result.Resource;
            }
        }

        public async Task<ResourceResponse<ResourceList<NodeResource>>> GetChildrenAsync(string tenantId, string spaceId, string nodeId, string continuationToken = null)
        {
            using (var client = CreateClient(serviceEndpoint))
            {
                return await client.GetChildrenAsync(tenantId, spaceId, nodeId);
            }
        }

        public async Task<ResourceResponse<SpaceResource>> GetSpace(string tenantId, string spaceId)
        {
            using (var client = CreateClient(serviceEndpoint))
            {
                return await client.GetSpaceAsync(tenantId, spaceId);
            }
        }

        public async Task DeleteChildren(string tenantId, string spaceId, string nodeId)
        {
            using (var client = CreateClient(serviceEndpoint))
            {
                await client.DeleteChildrenAsync(tenantId, spaceId, nodeId);
            }
        }

        public async Task DeleteNode(string tenantId, string spaceId, string nodeId)
        {
            using (var client = CreateClient(serviceEndpoint))
            {
                await client.DeleteNodeAsync(tenantId, spaceId, nodeId);
            }
        }

        public async Task UnShare(string tenantId, string nodeId, string sourceSpaceId, string spaceId)
        {
            using (var client = CreateClient(serviceEndpoint))
            {
                await client.UnShareNodeAsync(tenantId, sourceSpaceId, nodeId, spaceId.ToOrignalId(), "admin");
            }
        }

        public async Task Share(string tenantId, string nodeId, string sourceSpaceId, string spaceId)
        {
            using (var client = CreateClient(serviceEndpoint))
            {
                await client.ShareNodeAsync(tenantId, sourceSpaceId, nodeId, spaceId.ToOrignalId(), "admin");
            }
        }

        public async Task<SpaceResource> CreateSpace(string tenantId, string geoNetworkId, string spaceId, string rootNodeId)
        {
            using (var client = CreateClient(serviceEndpoint))
            {
                var r = await CreateSpaceIfNotExistAsync(client, geoNetworkId, tenantId, spaceId, rootNodeId);
                return r;
            }
        }
        private static async Task<SpaceResource> CreateSpaceIfNotExistAsync(ContentManagementClient cmsClient, string geoNetworkId, string tenantId, string spaceId, string rootNodeId)
        {
            ResourceResponse<SpaceResource> response;

            try
            {
                response = await cmsClient.GetSpaceAsync(tenantId, spaceId);
                if(response.Resource.RootNodeId != rootNodeId)
                {
                    response = await CreateSpace(cmsClient, geoNetworkId, tenantId, spaceId, rootNodeId);
                }
                try
                {
                    var node = await cmsClient.GetNodeAsync(tenantId, spaceId, response.Resource.RootNodeId);
                }
                catch (ContentManagementClientException ex)
                {
                    //no root node id
                    if (ex.IsNotFound && ex.Error?.ErrorInfo?.Data["objectType"] == "Node")
                    {
                        await cmsClient.DeleteSpaceAsync(tenantId, spaceId);
                        response = await cmsClient.CreateSpaceAsync(tenantId, response.Resource);
                    }
                }
            }
            catch (ContentManagementClientException ex)
            {
                //no space
                if (ex.IsNotFound && ex.Error?.ErrorInfo?.Data["objectType"] == "Space")
                {
                    response = await CreateSpace(cmsClient, geoNetworkId, tenantId, spaceId, rootNodeId);
                }
                else
                {
                    throw;
                }
            }

            return response.Resource;
        }

        private static async Task<ResourceResponse<SpaceResource>> CreateSpace(ContentManagementClient cmsClient, string geoNetworkId, string tenantId, string spaceId, string rootNodeId)
        {
            var space = new SpaceResource()
            {
                GeoNetworkId = geoNetworkId,
                TenantId = tenantId,
                RootNodeId = rootNodeId,
                Id = spaceId,
                Name = "CmsTool"
            };
            var response = await cmsClient.CreateSpaceAsync(tenantId, space);
            return response;
        }
    }


}
