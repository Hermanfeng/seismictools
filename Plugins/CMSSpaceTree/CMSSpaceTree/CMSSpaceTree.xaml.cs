﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using CMSTools.ShotGuidToOriginal;
using Seismic.ContentManagement.Model;
using Newtonsoft.Json;
using System.Text;
using Newtonsoft.Json.Converters;
using Seismic.Foreshock.Resource;
using Lusa.UI.Msic.MessageService;
using Lusa.UI.Msic.MessageService.MessageObject;
using Lusa.UI.Msic.FileService;
using Lusa.UI.WorkBenchContract.Controls.Pane;
using CMSSpaceTree;
using System.Net;
using CMSSpaceTree.CMSSpaceTree;

namespace CMSTools.CMSSpaceTree
{
    /// <summary>
    /// Interaction logic for CMSSpaceTree.xaml
    /// </summary>
    public partial class CMSSpaceTree : UserControl, IPanViewSerializer
    {
        public static CMSSpaceTree Instance;
        public CMSSpaceTree()
        {
            InitializeComponent();
            if (Instance == null)
            {
                Instance = this;
            }
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
        }

        public SyncFileEnv GetSyncFileEnv()
        {
            SyncFileEnv result = null;
            Dispatcher.Invoke(() =>
            {
                result = new SyncFileEnv()
                {
                    CmsEndpoit = this.CmsEndpoit.Text,
                    LibraryHost = this.LibraryHost.Text,
                    SpaceRootNodeId = this.RootNodeIdInput.Text,
                    SyncDeamonEndPoint = this.SyncDeamonEndPoint.Text,
                    UserName = this.UserName.Text,
                    SyncServiceEndPoint = this.SyncEndPoint.Text,
                    SpaceId = this.SpaceId.Text,
                    Tenant = this.TenantId.Text
                };
            });
            return result;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var step = "Get Root Node";
                SendProgressMessage(0, step);

                var tenant = this.TenantId.Text;
                var spaceId = this.SpaceId.Text.ToShortguid();
                var url = this.CmsEndpoit.Text;
                var tree = new PropertyNodeItem();
                var rootNode = new PropertyNodeItem()
                {
                    Name = tenant + "_" + spaceId + "(tenant_spaceId)",
                    IsFolder = true
                };
                tree.Children.Add(rootNode);
                rootNode.Parent = tree;
                var sp = await GetRootNode(url, tenant, spaceId);
                rootNode.Id = sp.RootNodeId;
                SendProgressMessage(30, step);
                var nodes = await GetNodeChildren(url, tenant, spaceId, sp.RootNodeId);
                var newNodes = nodes.Items.Select(node => ConvertToItem(node)).OrderByDescending(n=>n.IsFolder).ThenBy(n => n.Name);

                foreach (var newNode in newNodes)
                {
                    rootNode.Children.Add(newNode);
                    newNode.Parent = rootNode;
                    rootNode.IsExpanded = true;
                }
                this.treeView.ItemsSource = tree.Children;

                SendProgressMessage(100, step);

                SignalrSyncClient.Client.Start();

                RefreshMessageSpaceIds();
            }
            catch(Exception ex)
            {
                MessageService.Instance.SendMessage(ex);
            }
        }

        private void RefreshMessageSpaceIds()
        {
            var sid = this.SpaceId.Text;
            if(!string.IsNullOrEmpty(sid) && !SyncLogger.Message.IncludeSpaceIds.Contains(sid))
            {
                SyncLogger.Message.IncludeSpaceIds.Add(sid);
            }
        }

        private void SendProgressMessage(int progress, string msg)
        {
            MessageService.Instance.SendProgressMessage(progress, this, msg);
        }

        private PropertyNodeItem ConvertToItem(NodeResource node)
        {
            var item = new PropertyNodeItem()
            {
                Id = node.Id,
                IsFolder = node.Traits.FirstOrDefault(t => t.Key == "hierarchy").Value.Fields.FirstOrDefault(f => f.Key == "role").Value.ToString() == "Folder",
                Name = node.Traits.FirstOrDefault(t => t.Key == "name").Value.Fields.FirstOrDefault(f => f.Key == "name").Value.ToString(),
                OriginalNode = node,
            };

            item.Icon = item.HasTargetNodes ? "Images/sharedfileSource1.png" : (item.IsShortcut ? "Images/sharedfile1.png" : "Images/file.png");

            if (item.IsFolder)
            {
                var summaryTrait = node.Traits.FirstOrDefault(t => t.Key == "summary").Value;
                if (summaryTrait != null)
                {
                    item.ChildrenCount = int.Parse(summaryTrait.Fields.FirstOrDefault(f => f.Key == "fileCount").Value.ToString());
                }
                //item.Name = item.Name + $" (cc:{item.ChildrenCount})";
                if (item.ChildrenCount > 0)
                {
                    item.IsExpanded = true;
                }

                item.Icon = item.HasTargetNodes ? "Images/sharedfolderSource1.png" : (item.IsShortcut ? "Images/sharedfolder1.png" : "Images/folder1.png");
            }
            return item;
        }

        private async Task<SpaceResource> GetRootNode(string url, string tenant, string spaceId)
        {
            CmsClientHelper helper = CreateCmsClientHelper(url);
            var result = await helper.GetSpace(tenant, spaceId);
            return result.Resource;
        }

        private async Task<ResourceList<NodeResource>> GetNodeChildren(string url, string tenantId, string spaceId, string nodeId)
        {
            CmsClientHelper helper = CreateCmsClientHelper(url);
            try
            {
                var result = await helper.GetChildrenAsync(tenantId, spaceId, nodeId);
                return result.Resource;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private CmsClientHelper CreateCmsClientHelper(string url)
        {
            return new CmsClientHelper(url,this.TenantId.Text);
        }

        private async void treeView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var url = this.CmsEndpoit.Text;
            SendProgressMessage(0, "Retrieve Child Nodes");
            var tenant = this.TenantId.Text;
            var spaceId = this.SpaceId.Text.ToShortguid();
            var item = this.treeView.SelectedItem as PropertyNodeItem;
            if (item != null && item.IsFolder && !item.AlreadyRetrieved && !item.IsShortcut)
            {
                var cnodes = await GetNodeChildren(url, tenant, spaceId, item.Id);
                var newNodes = cnodes.Items.Select(node => ConvertToItem(node)).OrderByDescending(n => n.IsFolder).ThenBy(n => n.Name).ToList();
                foreach (var node in newNodes)
                {
                    item.Children.Add(node);
                    node.Parent = item;
                    item.IsExpanded = true;
                }
                item.AlreadyRetrieved = true;
            }
            SendProgressMessage(100, "Retrieve Child Nodes");
        }

        private bool IsShortcut(NodeResource node)
        {
            if(node==null)
            {
                return false;
            }

            var t = node.Traits.ContainsKey("correspondentSource") ? node.Traits["correspondentSource"] : null;
            return t != null;
        }

        private void treeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {

            var pitem = e.NewValue as PropertyNodeItem;
            if (pitem != null)
            {
                DictionaryPropertyGridAdapter gridItem = BuildGridShowItem(pitem);
                this.PropertyGrid1.SelectedObject = gridItem;
            }

        }

        private DictionaryPropertyGridAdapter BuildGridShowItem(PropertyNodeItem pitem)
        {
            var cateDic = new Dictionary<string, object>();
            var dic = new Dictionary<string, object>();
            var gridItem = new DictionaryPropertyGridAdapter(dic, cateDic);
            dic.Add(nameof(PropertyNodeItem.Id), pitem.Id ?? string.Empty);
            dic.Add("Id(Orignal)", pitem.Id?.ToOrignalId());
            dic.Add(nameof(PropertyNodeItem.Name), pitem.Name);
            dic.Add(nameof(PropertyNodeItem.ChildrenCount), pitem.ChildrenCount);
            dic.Add("IsShortcut", pitem.IsShortcut);

            if (pitem.OriginalNode != null)
            {
                foreach (var t in pitem.OriginalNode.Traits)
                {
                    var prefix = t.Key + "Trait";
                    foreach (var f in t.Value.Fields)
                    {
                        var key = f.Key;
                        if (!dic.ContainsKey(key))
                        {
                            if(key == "sourceNodeId")
                            {
                                dic.Add(key + "(OrignalId)", f.Value.ToString().ToOrignalId());
                                cateDic.Add(key + "(OrignalId)", prefix);
                            }
                            else if(key == "sourceSpaceId")
                            {
                                dic.Add(key + "(OrignalId)", f.Value.ToString().ToOrignalId());
                                cateDic.Add(key + "(OrignalId)", prefix);
                            }
                            else if(key == "targetNodes")
                            {
                                var count = 0;
                                foreach(var item in f.Value)
                                {
                                    count += 1;
                                    var tnId = item["targetNodeId"];
                                    var tsId = item["targetSpaceId"];
                                    dic.Add(key+count + "NodeId" + "(OrignalId)", tnId.ToString().ToOrignalId());
                                    cateDic.Add(key + count + "NodeId" + "(OrignalId)", prefix);
                                    dic.Add(key + count + "SpaceId" + "(OrignalId)", tsId.ToString().ToOrignalId());
                                    cateDic.Add(key + count + "SpaceId" + "(OrignalId)", prefix);
                                }
                            }
                            dic.Add(key, f.Value.ToString());
                            cateDic.Add(key, prefix);
                        }
                    }
                }
            }

            return gridItem;
        }

        public class StringToImageSourceConverter : IValueConverter
        {
            #region Converter

            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                string path = (string)value;
                if (!string.IsNullOrEmpty(path))
                {
                    return new BitmapImage(new Uri(path, UriKind.Absolute));
                }
                else
                {
                    return null;
                }

            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                return null;
            }
            #endregion
        }

        private async void Delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var url = this.CmsEndpoit.Text;
                var pitem = this.treeView.SelectedItem as PropertyNodeItem;

                if (pitem != null && pitem.OriginalNode != null)
                {
                    CmsClientHelper helper = CreateCmsClientHelper(url);
                    var isShortcut = pitem.IsShortcut;

                    if (pitem.IsFolder)
                    {
                        if (!isShortcut)
                        {
                            await helper.DeleteChildren(this.TenantId.Text, this.SpaceId.Text.ToShortguid(), pitem.Id);
                        }
                        //root node can not delete 
                        if (pitem.Parent != null)
                        {
                            await helper.DeleteNode(this.TenantId.Text, this.SpaceId.Text.ToShortguid(), pitem.Id);
                        }
                    }
                    else
                    {
                        await helper.DeleteNode(this.TenantId.Text, this.SpaceId.Text.ToShortguid(), pitem.Id);
                    }
                    MessageService.Instance.SendMessage($"Delete node {pitem.Id} sucessfull");
                    if (pitem.Parent != null)
                    {
                        pitem.Parent.Children.Remove(pitem);
                    }
                }
                else
                {
                    MessageService.Instance.SendMessage($"Can not delete current selected node sucessfull", MessageType.ERROR);
                }
            }
            catch(Exception ex)
            {
                MessageService.Instance.SendMessage(ex);
            }
        }

        private async void CreateSpace_Click(object sender, RoutedEventArgs e)
        {
            var tenant = this.TenantId.Text;
            var spaceId = this.SpaceId.Text.ToShortguid();
            var rootNode = this.RootNodeIdInput.Text.ToShortguid();
            var geoId = this.GeoIdInput.Text;
            var url = this.CmsEndpoit.Text;
            var helper = this.CreateCmsClientHelper(url);

            var space = await helper.CreateSpace(tenant, geoId, spaceId, rootNode);
            MessageBox.Show("Create successfully!");
        }

        private void SyncRootNode_Click(object sender, RoutedEventArgs e)
        {
            var syncEndpoit = this.SyncEndPoint.Text;
            var tenant = this.TenantId.Text;
            var spaceId = this.SpaceId.Text;
            var rootNode = this.RootNodeIdInput.Text;
            var url = "/api/syncservice/v1/jobs";
            var syncJob = new SyncJob() { SpaceId = spaceId, RootNodeId = rootNode };
            var json = JsonConvert.SerializeObject(syncJob);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var httpClient = new HttpClient() { BaseAddress = new Uri(syncEndpoit) };
            httpClient.PostAsync(url, content);
        }

        string IPanViewSerializer.Serializer()
        {
            var splitor = "||||";
            StringBuilder sb = new StringBuilder();
            sb.Append(this.CmsEndpoit.Text);
            sb.Append(splitor);
            sb.Append(this.TenantId.Text);
            sb.Append(splitor);
            sb.Append(this.SpaceId.Text);
            sb.Append(splitor);
            sb.Append(this.RootNodeIdInput.Text);
            sb.Append(splitor);
            sb.Append(this.SyncEndPoint.Text);

            sb.Append(splitor);
            sb.Append(this.SyncDeamonEndPoint.Text);
            sb.Append(splitor);
            sb.Append(this.UserName.Text);
            sb.Append(splitor);
            sb.Append(this.LibraryHost.Text);
            sb.Append(splitor);
            sb.Append(this.FolderPath.Text);
            sb.Append(splitor);
            sb.Append(this.expander.IsExpanded);

            sb.Append(splitor);
            sb.Append(this.GeoIdInput.Text);

            return sb.ToString();
        }

        void IPanViewSerializer.DeSerializer(string data)
        {
            var splitor = "||||";
            var lines = data.Split(new string[] { splitor }, StringSplitOptions.None);
            this.CmsEndpoit.Text = lines[0];
            this.TenantId.Text = lines[1];
            this.SpaceId.Text = lines[2];
            this.RootNodeIdInput.Text = lines[3];
            this.SyncEndPoint.Text = lines[4];
            if(lines.Length > 5)
            {
                this.SyncDeamonEndPoint.Text = lines[5];
                this.UserName.Text = lines[6];
                this.LibraryHost.Text = lines[7];
                this.FolderPath.Text = lines[8];

            }
            if(lines.Length > 9)
            {
                if(bool.TryParse(lines[9],out var isExpanded))
                {
                    this.expander.IsExpanded = isExpanded;
                }
            }

            if (lines.Length > 10)
            {
                this.GeoIdInput.Text = lines[10];
            }

            RefreshMessageSpaceIds();
        }

        public class SyncJob
        {
  
            public string SpaceId { get; set; }

            public string RootNodeId { get; set; }

        }

        private async void Share_Click(object sender, RoutedEventArgs e)
        {
            var url = this.CmsEndpoit.Text;
            var tenant = this.TenantId.Text;
            var spaceId = this.SpaceId.Text.ToShortguid();
            var pitem = this.treeView.SelectedItem as PropertyNodeItem;

            if (pitem != null && pitem.OriginalNode != null)
            {
                var ids = pitem.TargetContents?.Select(c => c.UserId.ToOrignalId()).ToList();
                var shareWindow = new ShareWindow(spaceIds: ids);
                var result = shareWindow.ShowDialog();
                if(result.HasValue && result.Value)
                {
                    var helper = CreateCmsClientHelper(this.CmsEndpoit.Text);
                    var sids = shareWindow.SpaceIds;
                    foreach (var sid in sids)
                    {
                        var oid = sid.ToShortguid();
                        if (!ids.Contains(sid))
                        {
                            await helper.Share(tenant, pitem.Id, spaceId, oid);
                        }
                    }
                }
            }
        }

        private async void Unshare_Click(object sender, RoutedEventArgs e)
        {
            var url = this.CmsEndpoit.Text;
            var tenant = this.TenantId.Text;
            var spaceId = this.SpaceId.Text.ToShortguid();
            var pitem = this.treeView.SelectedItem as PropertyNodeItem;

            if (pitem != null && pitem.OriginalNode != null)
            {
                var helper = CreateCmsClientHelper(this.CmsEndpoit.Text);
                if (pitem.IsShortcut)
                {
                    await helper.UnShare(tenant, pitem.SourceContent.ContentId, pitem.SourceContent.UserId, spaceId);
                    pitem.Parent.Children.Remove(pitem);
                }
                else if (pitem.HasTargetNodes)
                {
                    var shareWindow = new ShareWindow(true, pitem.TargetContents.Select(c=>c.UserId.ToOrignalId()).ToList());
                    var result = shareWindow.ShowDialog();
                    if (result.HasValue && result.Value)
                    {
                        foreach(var uid in shareWindow.SpaceIds)
                        {
                            await helper.UnShare(tenant, pitem.Id, spaceId, uid.ToShortguid());
                        }
                    }
                    
                }
            }
        }

        private void contextMenu_Opened(object sender, RoutedEventArgs e)
        {
            this.contextMenu.DataContext = this.treeView.SelectedItem;

        }

        private async void CreateFile_Click(object sender, RoutedEventArgs e)
        {
            var url = this.CmsEndpoit.Text;
            var tenant = this.TenantId.Text;
            var spaceId = this.SpaceId.Text.ToShortguid();
            var pitem = this.treeView.SelectedItem as PropertyNodeItem;

            if (pitem != null)
            {
                var helper = CreateCmsClientHelper(this.CmsEndpoit.Text);
                var node = await helper.CreateVersionedFileAsync(tenant, spaceId, pitem.Id);
                var newItem = ConvertToItem(node);
                pitem.Children.Add(newItem);
            }
            else
            {
                throw new Exception("No valid parent node.");
            }
        }

        private async void CreateFolder_Click(object sender, RoutedEventArgs e)
        {
            var url = this.CmsEndpoit.Text;
            var tenant = this.TenantId.Text;
            var spaceId = this.SpaceId.Text.ToShortguid();
            var pitem = this.treeView.SelectedItem as PropertyNodeItem;

            if (pitem != null)
            {
                var helper = CreateCmsClientHelper(this.CmsEndpoit.Text);
                var node = await helper.CreateFolderAsync(tenant, spaceId, pitem.Id);
                var newItem = ConvertToItem(node);
                pitem.Children.Add(newItem);
            }
            else
            {
                throw new Exception("No valid parent node.");
            }
        }

        private async void SyncFileSystem_Click(object sender, RoutedEventArgs e)
        {
            //var config = await GetSyncConfiguration();
            //if (config == null)
            var config = await CreateSyncConfiguration();
            var path = config.SpaceFoldrMaps[0].FolderPath;
            MessageService.Instance.SendHyperLinkMessage($"Start to sync file with cms, file system path {path}", path);
            var temptoken = await GetDeamonServiceToken();
            MessageService.Instance.SendMessage($"Sync token : {temptoken.AccessToken}");
            await SetDeamonServiceToken(temptoken);
            await StartSyncFileSystem();
        }



        private async Task StartSyncFileSystem()
        {
            var syncEndpoit = this.SyncDeamonEndPoint.Text;
            var tenant = this.TenantId.Text;
            var spaceId = this.SpaceId.Text;
            var rootNode = this.RootNodeIdInput.Text;
            var url = $"/api/syncDaemon/v1/sync/start";
            var httpClient = new HttpClient() { BaseAddress = new Uri(syncEndpoit) };
            var response = await httpClient.PostAsync(url, null);
            if(!response.IsSuccessStatusCode && response.Content!=null)
            {
                var error = await response.Content.ReadAsStringAsync();
                MessageService.Instance.SendMessage(new Exception(error));
            }
        }

        private async Task<SyncTokenObject> GetDeamonServiceToken()
        {
            var tenant = this.TenantId.Text;
            var userName = this.UserName.Text;
            var authServerUrl = await GetAuthServerUrl();
            var url = $"/api/syncDaemon/v1/sync/start";
            var token = CmsClientHelper.TokenMapInstance.GetToken(authServerUrl+userName, tenant);
            return new SyncTokenObject { AccessToken = token, ExpiredIn= int.MaxValue};
        }

        private async Task<string> GetAuthServerUrl()
        {
            var tenant = this.TenantId.Text;
            var libraryUrl = $"https://{tenant}.{LibraryHost.Text}/.well-known/authentication";
            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(libraryUrl);
            var result = await response.Content.ReadAsAsync<dynamic>();
            var url = (string)result.BaseUrl.ToString();
            //return url.Substring(0, url.Length - 10 - tenant.Length);
            return url;
        }

        private async Task SetDeamonServiceToken(SyncTokenObject token)
        {
            var syncEndpoit = this.SyncDeamonEndPoint.Text;
            var url = $"/api/syncDaemon/v1/auth/token";
            var httpClient = new HttpClient() { BaseAddress = new Uri(syncEndpoit) };
            var json = JsonConvert.SerializeObject(token);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await httpClient.PostAsync(url, content);
            if (!response.IsSuccessStatusCode && response.Content != null)
            {
                var error = await response.Content.ReadAsStringAsync();
                MessageService.Instance.SendMessage(new Exception(error));
            }
        }

        private async Task<SyncConfiguration> GetSyncConfiguration()
        {
            var syncEndpoit = this.SyncDeamonEndPoint.Text;
            var url = $"/api/syncDaemon/v1/configurations/configuration";
            var httpClient = new HttpClient() { BaseAddress = new Uri(syncEndpoit) };
            var response = await httpClient.GetAsync(url);
            if(response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<SyncConfiguration>();
            }
            return null;
        }

        private async Task<SyncConfiguration> CreateSyncConfiguration()
        {
            var syncEndpoit = this.SyncDeamonEndPoint.Text;
            var tenant = this.TenantId.Text;
            var spaceId = this.SpaceId.Text;
            var folderPath = FileService.Instance.EnsureAbsoluteDirectory($"SyncFolder\\{spaceId}");
            var rootNode = this.RootNodeIdInput.Text;
            var url = $"/api/syncDaemon/v1/configurations/configuration";
            var httpClient = new HttpClient() { BaseAddress = new Uri(syncEndpoit) };
            var syncConfig = new SyncConfiguration()
            {
                CMSEndpoint = this.CmsEndpoit.Text,
                Tenant = tenant,
                UserName = UserName.Text,
                LibraryHost = LibraryHost.Text,
                SpaceFoldrMaps = new SpaceConfiguration[]
                {
                    new SpaceConfiguration()
                    {
                        SpaceId = spaceId,
                        Direction =  SyncDirection.Bidirectional,
                        FolderPath = folderPath
                    }
                }
            };
            var json = JsonConvert.SerializeObject(syncConfig);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            await httpClient.PostAsync(url, content);
            return syncConfig;
        }
    }

    public class BoolToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TrewViewVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value!=null && ((TreeView)value).SelectedItem != null ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class SyncConfiguration
    {
        [JsonProperty("cmsEndpoint")]
        public string CMSEndpoint { get; set; }

        [JsonProperty("tenant")]
        public string Tenant { get; set; }

        [JsonProperty("libraryHost")]
        public string LibraryHost { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("spaceFolderMaps")]
        public SpaceConfiguration[] SpaceFoldrMaps { get; set; }
    }

    public class SpaceConfiguration
    {
        [JsonProperty("spaceId")]
        public string SpaceId { get; set; }

        [JsonProperty("folderPath")]
        public string FolderPath { get; set; }

        [JsonProperty("direction")]
        public SyncDirection Direction { get; set; }

        /// <summary>
        /// Key is node id and value is selected status
        /// </summary>
        [JsonProperty("selectedNodes")]
        public Dictionary<string, string> SelectedNodes { get; set; }
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum SyncDirection
    {
        Download,
        Upload,
        Bidirectional,
    }

    public class SyncTokenObject
    {
        [JsonProperty("expiredIn")]
        public int ExpiredIn { get; set; }
        [JsonProperty("accessToken")]
        public string AccessToken { get; set; }
    }

    public class SyncFileEnv
    {
        public string SyncDeamonEndPoint { get; set; }

        public string CmsEndpoit { get; set; }

        public string SpaceRootNodeId { get; set; }

        public string UserName { get; set; }

        public string LibraryHost { get; set; }

        public string SpaceId { get; set; }

        public string SyncServiceEndPoint { get; set; }

        public string Tenant { get; set; }
    }
}
