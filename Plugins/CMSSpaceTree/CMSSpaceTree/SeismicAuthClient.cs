﻿using Seismic.Foreshock.Resource;

namespace CMSTools
{
    //public class SeismicAuthClient : IAccessTokenClient
    //{
    //    private readonly TokenClientConfiguration configuration;

    //    public SeismicAuthClient(TokenClientConfiguration configuration)
    //    {
    //        this.configuration = configuration;
    //    }

    //    public string GetAccessToken(bool bypassCaching = false)
    //    {
    //        var url = $"{configuration.AuthServiceUrl}/tenants/{configuration.Tenant}/connect/token";
    //        var tokenClient = new TokenClient(url, configuration.ClientId, configuration.ClientSecret);
    //        var response = tokenClient.RequestClientCredentialsAsync(configuration.ClientScope).GetAwaiter().GetResult();
    //        return response.AccessToken;
    //    }
    //}

    public class CustomTokenClient : IAccessTokenClient
    {
        private readonly string token;

        public CustomTokenClient(string token)
        {
            this.token = token;
        }

        public string GetAccessToken(bool bypassCaching = false)
        {
            return token;
        }
    }

    public class TokenClientConfiguration
    {
        public TokenRetrieveType TokenRetrieveType { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string AuthServiceUrl
        {
            get;
            set;
        }

        public string ClientId
        {
            get;
            set;
        }

        public string ClientSecret
        {
            get;
            set;
        }

        public string ClientScope
        {
            get;
            set;
        }
    }

    public enum TokenRetrieveType
    {
        ClientCridential,
        UserOwnerPassword
    }


}
