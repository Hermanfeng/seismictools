﻿using System;
using System.Collections;
using System.ComponentModel;

namespace CMSTools.CMSSpaceTree
{
    public partial class CMSSpaceTree
    {
        class DictionaryPropertyDescriptor : PropertyDescriptor
        {
            //PropertyDescriptor provides 3 constructors. We want the one that takes a string and an array of attributes:

            IDictionary _dictionary;
            object _key;
            private readonly string category;

            internal DictionaryPropertyDescriptor(IDictionary d, object key, string category)
                : base(key.ToString(), null)
            {
                _dictionary = d;
                _key = key;
                this.category = category;
            }

            public override string Category => category;
            //The attributes are used by PropertyGrid to organise the properties into categories, to display help text and so on. We don't bother with any of that at the moment, so we simply pass null.

            //The first interesting member is the PropertyType property. We just get the object out of the dictionary and ask it:

            public override Type PropertyType
            {
                get { return _dictionary[_key].GetType(); }
            }
            //If you knew that all of your values were strings, for example, you could just return typeof(string).

            //Then we implement SetValue and GetValue:

            public override void SetValue(object component, object value)
            {
                _dictionary[_key] = value;
            }

            public override object GetValue(object component)
            {
                return _dictionary[_key];
            }
            public override bool IsReadOnly
            {
                get { return false; }
            }

            public override Type ComponentType
            {
                get { return null; }
            }

            public override bool CanResetValue(object component)
            {
                return false;
            }

            public override void ResetValue(object component)
            {
            }

            public override bool ShouldSerializeValue(object component)
            {
                return false;
            }
        }
    }
}
