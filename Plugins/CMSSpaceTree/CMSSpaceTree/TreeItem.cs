﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Seismic.ContentManagement.Model;

namespace CMSTools
{
    public class PropertyNodeItem : INotifyPropertyChanged
    {
        private bool _isExpanded;

        public string Icon { get; set; }
        public string EditIcon { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }

        public bool IsFolder { get; set; }

        public int ChildrenCount { get; set; }

        public bool AlreadyRetrieved { get; set; }

        public bool CanDelete { get { return !this.IsShortcut && !this.HasTargetNodes;  } }
        public bool CanShare { get { return !this.IsShortcut && OriginalNode!=null; } }
        public bool CanUnShare { get { return this.IsShortcut || HasTargetNodes; } }

        public bool IsShortcut
        {
            get
            {
                return SourceContent != null;
            }
        }

        SharedContent _sourceContent;
        public SharedContent SourceContent {

            get
            {
                if (_sourceContent == null && OriginalNode != null)
                {
                    var node = OriginalNode;
                    var t = node.Traits.ContainsKey("correspondentSource") ? node.Traits["correspondentSource"] : null;
                    if (t != null)
                    {
                        _sourceContent = new SharedContent()
                        {
                            ContentId = t.Fields["sourceNodeId"].ToString(),
                            UserId = t.Fields["sourceSpaceId"].ToString(),
                        };
                    }
                }
                return _sourceContent;
            }
        }

        List<SharedContent> _targetContents;
        public List<SharedContent> TargetContents
        {
            get
            {
                if (_targetContents == null && OriginalNode != null)
                {
                    _targetContents = new List<SharedContent>();
                    var node = OriginalNode;
                    var t = node.Traits.ContainsKey("correspondentTarget") ? node.Traits["correspondentTarget"]?.Fields["targetNodes"] : null;
                    if (t != null && t.Count() > 0)
                    {
                        foreach(var c in t)
                        {
                            var tnid = c["targetNodeId"].ToString();
                            var tsid = c["targetSpaceId"].ToString();
                            _targetContents.Add(new SharedContent { ContentId = tnid, UserId=tsid });
                        }
                    }
                }
                return _targetContents;
            }
        }

        public bool HasTargetNodes
        {
            get
            {
                var tcs = TargetContents;
                return tcs!=null && tcs.Count>0;
            }
        }

        public NodeResource OriginalNode { get; set; }
        public ObservableCollection<PropertyNodeItem> Children { get; set; }

        public PropertyNodeItem Parent { get; set; }

        public PropertyNodeItem()
        {
            Children = new ObservableCollection<PropertyNodeItem>();
        }

        public bool IsExpanded { get => _isExpanded;
           set 
           {
                var oldValue = _isExpanded;
                _isExpanded = value;
                if (oldValue!= _isExpanded && PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("IsExpanded"));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class SharedContent
    {
        public string ContentId { get; set; }
        public string UserId { get; set; }
    }

    public class PropertyTree
    {
        public PropertyTree()
        {
            Children = new ObservableCollection<PropertyNodeItem>();
        }
        public ObservableCollection<PropertyNodeItem> Children { get; set; }
    }
}
