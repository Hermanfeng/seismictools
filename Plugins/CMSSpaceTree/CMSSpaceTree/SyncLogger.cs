﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;
using Lusa.UI.Msic.MessageService;
using Lusa.UI.Msic.MessageService.MessageObject;

namespace CMSSpaceTree.CMSSpaceTree
{
    public class SyncLogger
    {
        //public const string LogentriesLogToken = "e08b955b-cb43-49da-98fe-632644429b83";
        //public const string LogentriesLogQueryToken = "ecce1686-1f80-493a-a391-72f3b22fa4b8";
        //public string LogentriesUrlPath = "https://rest.logentries.com/query/logs";
        //public string AuthHeader = "x-api-key";
        //public const string SyncLogFileDir = "";
        //private DateTime lastTimeStamp = DateTime.Now;
        //public string filePath = Path.Combine(SyncLogFileDir, $"synclog_{System.DateTime.Today.Date.ToShortDateString()}.tet");
        NamedPipeClientStream pipe;
        public SyncLogger(string name)
        {
            Task.Run(() =>
            {
                try
                {
                    pipe = new NamedPipeClientStream(".", name, PipeDirection.In, PipeOptions.None);
                    
                    //pipe.ReadMode = PipeTransmissionMode.Message;
                    var data = new byte[655350];

                    while (true)
                    {
                        try
                        {
                            if (!pipe.IsConnected)
                            {
                                pipe.Connect(10 * 1000);
                            }
                            var count = pipe.Read(data, 0, data.Length);
                            if (count > 0)
                            {
                                string message = UTF8Encoding.UTF8.GetString(data, 0, count);
                                var msg = new Message(message);
                                if (!msg.IsIgnore)
                                {
                                    MessageService.Instance.SendMessage(msg.Msg, msg.MsgType, customColor:msg.CustomColor);
                                }
                                Thread.Sleep(100);
                            }
                        }
                        catch(Exception ex)
                        {
                            //MessageService.Instance.SendMessage(ex);
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    MessageService.Instance.SendMessage(ex);
                }
            });
        }

        public class Message
        {
            public static List<string> IncludeSpaceIds { get; } = new List<string>();

            public Message(string input)
            {
                var tokens = input.Split(new string[] { "[", "]" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var token in tokens)
                {
                    if (token.StartsWith("s:"))
                    {
                        Source = token.Substring(2, token.Length - 2);
                    }
                    else if(token.StartsWith("l:"))
                    {
                        var type = token.Substring(2, token.Length - 2);
                        if (type == "Debug")
                        {
                            MsgType = MessageType.DEBUG;
                        }
                        else if(type == "Error" || token == "Fatal")
                        {
                            MsgType = MessageType.ERROR;
                        }
                        else if(type == "Warning")
                        {
                            MsgType = MessageType.WARNING;
                        }
                        else
                        {
                            MsgType = MessageType.INFO;
                        }
                    }
                    else if (token.StartsWith("sp:"))
                    {
                        SpaceId = token.Substring(3, token.Length - 3);
                    }
                    else if (token.StartsWith("u:"))
                    {
                        UserName = token.Substring(2, token.Length - 2);
                    }
                    else
                    {
                        Msg = token;
                    }
                }

                if(Source == "SyncJobExecutor" && MsgType < MessageType.WARNING)
                {
                    CustomColor = Color.FromRgb(0, 0, 255);
                }
                if (!string.IsNullOrEmpty(SpaceId))
                {
                    Msg = $"[SpaceId: { SpaceId }]{Msg}";
                }
                else if(!string.IsNullOrEmpty(UserName))
                {
                    Msg = $"[UserName: { UserName }]{Msg}";
                }

                if(!IncludeSpaceIds.Contains(SpaceId))
                {
                    IsIgnore = true;
                }
            }

            public string Source { get; set; }

            public string UserName { get; set; }

            public MessageType MsgType { get; set; }

            public Color? CustomColor { get; set; }

            public string Msg { get; set; }

            public string SpaceId { get; set; }

            public bool IsIgnore { get; set; }
        }

        //private void Log(object state)
        //{
        //    var httpClient = new HttpClient();
        //    httpClient.DefaultRequestHeaders.Add(AuthHeader, LogentriesLogQueryToken);
        //    var from = lastTimeStamp;
        //    var to = lastTimeStamp.Add(TimeSpan.FromSeconds(3));
        //    lastTimeStamp = to;
        //    var request = new LogentriesQueryParam()
        //    {
        //        Logs = new string[] { LogentriesLogToken },
        //        Statement = "where(/.*/)",
        //        From = ConvertDateTimeInt(from),
        //        To = ConvertDateTimeInt(to)
        //    };
        //    var json = JsonConvert.SerializeObject(request);
        //    var content = new StringContent(json, Encoding.UTF8, "application/json");
        //    var response = httpClient.PostAsync(LogentriesUrlPath, content);
        //    var msg = response.Result.Content.ReadAsStringAsync().GetAwaiter().GetResult();


        //}

        //private void Log(object state)
        //{
        //    var dir = @"C:\Work\ContentManagementSystem\src\SyncCore\src\Seismic.Sync.Service.Server\bin\Debug\netcoreapp2.1";
        //    var name = $"synclog_{System.DateTime.Today.Date.ToShortDateString()}.txt".Replace(@"/", "_");
        //    var logfilePath = System.IO.Path.Combine(dir, name);
        //    var path = System.IO.File.ReadLines()


        //}

        public static int ConvertDateTimeInt(System.DateTime time)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            return (int)(time - startTime).TotalSeconds;
        }

        public class LogentriesQueryParam
        {
            public string[] Logs { get; set; }

            public string Statement { get; set; }

            public long From { get; set; }

            public long To { get; set; }
        }
    }
}
