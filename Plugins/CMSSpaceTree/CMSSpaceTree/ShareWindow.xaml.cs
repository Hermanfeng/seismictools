﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Linq;

namespace CMSTools
{
    /// <summary>
    /// Interaction logic for SplashWindow.xaml
    /// </summary>
    public partial class ShareWindow : Window
    {
        public ShareWindow(bool isUnshare=false, List<string> spaceIds=null)
        {
            InitializeComponent();
            this.Share.Content = isUnshare ? "Unshare" : "Share";
            if (spaceIds != null && spaceIds.Count > 0)
            {
                this.SharedSpaceId.Text = string.Join(","+ Environment.NewLine, spaceIds);
            }
        }

        public List<string> SpaceIds
        {
            get
            {
                return new List<string>(this.SharedSpaceId.Text.Split(new string[] {",", Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries));
            }
        }

        private void Share_Click(object sender, RoutedEventArgs e)
        {
            var spaceId = this.SharedSpaceId.Text;
            if (!string.IsNullOrEmpty(spaceId))
            {
                this.DialogResult = true;
            }
        }
    }
}
