﻿using System.Windows;
using Lusa.UI.WorkBenchContract.Controls.Pane;

namespace CMSSpaceTree
{
    public class CMSSpaceTreePaneProvider1 : IPaneViewDescriptorProvider
    {
        PaneViewDescriptor IPaneViewDescriptorProvider.Pane
        {
            get
            {
                return new PaneViewDescriptor(typeof(CMSTools.CMSSpaceTree.CMSSpaceTree)) { Header = "CMSSpaceTree1", IsDocument = true };
            }
        }
    }

    public class CMSSpaceTreePaneProvider2 : IPaneViewDescriptorProvider
    {
        PaneViewDescriptor IPaneViewDescriptorProvider.Pane
        {
            get
            {
                return new PaneViewDescriptor(typeof(CMSTools.CMSSpaceTree.CMSSpaceTree)) { Header = "CMSSpaceTree2", IsDocument = true };
            }
        }
    }

    public class CMSSpaceTreePaneProvider3 : IPaneViewDescriptorProvider
    {
        PaneViewDescriptor IPaneViewDescriptorProvider.Pane
        {
            get
            {
                return new PaneViewDescriptor(typeof(CMSTools.CMSSpaceTree.CMSSpaceTree)) { Header = "CMSSpaceTree3", IsDocument = true };
            }
        }
    }
}
