﻿using System;
using System.Collections.Generic;
using IdentityModel.Client;

namespace CMSTools
{
    public class TokenMap
    {
        private  Dictionary<string, TokenResponse> ServiceTokens = new Dictionary<string, TokenResponse>();
        private Dictionary<string, TokenClientConfiguration> TokenClientConfigurations = new Dictionary<string, TokenClientConfiguration>();
        public string GetToken(string serviceEndponit,string tenant)
        {
            var key = serviceEndponit + tenant;
            if(ServiceTokens.ContainsKey(key))
            {
                var expiredTime = GetTime(ServiceTokens[key].ExpiresIn.ToString());
                if(expiredTime - DateTime.UtcNow > TimeSpan.FromMinutes(5))
                {
                    return ServiceTokens[key].AccessToken;
                }
                else
                {
                    ServiceTokens.Remove(key);
                }
            }
            if(TokenClientConfigurations.ContainsKey(serviceEndponit))
            {
                var configuration = TokenClientConfigurations[serviceEndponit];
                var url = $"{configuration.AuthServiceUrl}/tenants/{tenant}/connect/token";
                var tokenClient = new TokenClient(url, configuration.ClientId, configuration.ClientSecret);
                if (configuration.TokenRetrieveType == TokenRetrieveType.ClientCridential)
                {
                    var response = tokenClient.RequestClientCredentialsAsync(configuration.ClientScope).GetAwaiter().GetResult();
                    ServiceTokens.Add(key, response);
                    return response.AccessToken;
                }
                else
                {
                    var response = tokenClient.RequestResourceOwnerPasswordAsync(configuration.UserName, configuration.Password, configuration.ClientScope).GetAwaiter().GetResult();
                    ServiceTokens.Add(key, response);
                    return response.AccessToken;
                }
            }
            return "FakeToken";
        }

        public TokenMap()
        {
            TokenClientConfigurations.Add("https://foreshock-dev01-westus.seismic.com/cms", new TokenClientConfiguration()
            {
                AuthServiceUrl = "https://auth-qa01-westus.seismic-dev.com",
                ClientId = "f57d1df4-4dbe-41c2-8f50-e8d43c41968c",
                ClientSecret = "59d1cfd8-26b8-4cea-81d8-c503c6c92559",
                ClientScope = "library cms_api",
            });

            TokenClientConfigurations.Add("https://foreshock-dev01-westus.seismic.com", new TokenClientConfiguration()
            {
                AuthServiceUrl = "https://auth-qa01-westus.seismic-dev.com",
                ClientId = "f57d1df4-4dbe-41c2-8f50-e8d43c41968c",
                ClientSecret = "59d1cfd8-26b8-4cea-81d8-c503c6c92559",
                ClientScope = "library cms_api",
            });

            TokenClientConfigurations.Add("https://foreshock-qa01-westus.seismic.com", new TokenClientConfiguration()
            {
                AuthServiceUrl = "https://auth-qa01-eastasia.seismic-dev.com",
                ClientId = "2ea1467e-8e0d-498f-a117-911bef98a878",
                ClientSecret = "3263f6e5-ba2c-4e4c-bf31-a672b340ea50",
                ClientScope = "library cms_api",
            });

            TokenClientConfigurations.Add("https://auth-dev02-eastasia.seismic-dev.com/tenants/eric/admin", new TokenClientConfiguration()
            {
                AuthServiceUrl = "https://auth-dev02-eastasia.seismic-dev.com",
                ClientId = "5eaf7058-611c-42cd-b9f9-8d4b81436b1c",
                ClientSecret = "5c9bd75c-fdef-4809-b9d5-fb35e00142ef",
                ClientScope = "library cms_api",
                UserName = "admin",
                Password = "Foreshock!1234.",
                TokenRetrieveType = TokenRetrieveType.UserOwnerPassword
            });
            TokenClientConfigurations.Add("https://auth-dev02-eastasia.seismic-dev.com/tenants/eric/herman", new TokenClientConfiguration()
            {
                AuthServiceUrl = "https://auth-dev02-eastasia.seismic-dev.com",
                ClientId = "5eaf7058-611c-42cd-b9f9-8d4b81436b1c",
                ClientSecret = "5c9bd75c-fdef-4809-b9d5-fb35e00142ef",
                ClientScope = "library cms_api",
                UserName = "herman",
                Password = "Foreshock!1234.",
                TokenRetrieveType = TokenRetrieveType.UserOwnerPassword
            });
        }

        /// <summary>  
        /// Unix时间戳转为C#格式时间  
        /// </summary>  
        /// <param name="timeStamp">Unix时间戳格式,例如1482115779</param>  
        /// <returns>C#格式时间</returns>  
        public static DateTime GetTime(string timeStamp)
        {
            DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            long lTime = long.Parse(timeStamp + "0000000");
            TimeSpan toNow = new TimeSpan(lTime);
            return dtStart.Add(toNow);
        }


        /// <summary>  
        /// DateTime时间格式转换为Unix时间戳格式  
        /// </summary>  
        /// <param name="time"> DateTime时间格式</param>  
        /// <returns>Unix时间戳格式</returns>  
        public static int ConvertDateTimeInt(System.DateTime time)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            return (int)(time - startTime).TotalSeconds;
        }
    }


}
