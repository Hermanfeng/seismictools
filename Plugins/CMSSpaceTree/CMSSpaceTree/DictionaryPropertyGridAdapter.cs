﻿using System;
using System.Collections;
using System.ComponentModel;

namespace CMSTools.CMSSpaceTree
{
    public partial class CMSSpaceTree
    {
        class DictionaryPropertyGridAdapter : ICustomTypeDescriptor
        {
            IDictionary _dictionary;
            private readonly IDictionary catedic;

            public DictionaryPropertyGridAdapter(IDictionary d,IDictionary catedic)
            {
                _dictionary = d;
                this.catedic = catedic;
            }
            //Three of the ICustomTypeDescriptor methods are never called by the property grid, but we'll stub them out properly anyway:

            public string GetComponentName()
            {
                return TypeDescriptor.GetComponentName(this, true);
            }

            public EventDescriptor GetDefaultEvent()
            {
                return TypeDescriptor.GetDefaultEvent(this, true);
            }

            public string GetClassName()
            {
                return TypeDescriptor.GetClassName(this, true);
            }
            //Then there's a whole slew of methods that are called by PropertyGrid, but we don't need to do anything interesting in them:

            public EventDescriptorCollection GetEvents(Attribute[] attributes)
            {
                return TypeDescriptor.GetEvents(this, attributes, true);
            }

            EventDescriptorCollection System.ComponentModel.ICustomTypeDescriptor.GetEvents()
            {
                return TypeDescriptor.GetEvents(this, true);
            }

            public TypeConverter GetConverter()
            {
                return TypeDescriptor.GetConverter(this, true);
            }

            public object GetPropertyOwner(PropertyDescriptor pd)
            {
                return _dictionary;
            }

            public AttributeCollection GetAttributes()
            {
                return TypeDescriptor.GetAttributes(this, true);
            }

            public object GetEditor(Type editorBaseType)
            {
                return TypeDescriptor.GetEditor(this, editorBaseType, true);
            }

            public PropertyDescriptor GetDefaultProperty()
            {
                return null;
            }

            PropertyDescriptorCollection
                System.ComponentModel.ICustomTypeDescriptor.GetProperties()
            {
                return ((ICustomTypeDescriptor)this).GetProperties(new Attribute[0]);
            }
            //Then the interesting bit. We simply iterate over the IDictionary, creating a property descriptor for each entry:

            public PropertyDescriptorCollection GetProperties(Attribute[] attributes)
            {
                ArrayList properties = new ArrayList();
                foreach (DictionaryEntry e in _dictionary)
                {
                    var cate = catedic.Contains(e.Key) ? catedic[e.Key].ToString() : "Common";
                    properties.Add(new DictionaryPropertyDescriptor(_dictionary, e.Key, cate));
                }

                PropertyDescriptor[] props =
                    (PropertyDescriptor[])properties.ToArray(typeof(PropertyDescriptor));

                return new PropertyDescriptorCollection(props);
            }


        }
    }
}
