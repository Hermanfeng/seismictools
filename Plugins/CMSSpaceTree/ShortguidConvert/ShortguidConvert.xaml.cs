﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CMSTools.ShotGuidToOriginal;

namespace CMSSpaceTree.ShortguidConvert
{
    /// <summary>
    /// Interaction logic for ShortguidConvert.xaml
    /// </summary>
    public partial class ShortguidConvert : UserControl
    {
        public ShortguidConvert()
        {
            InitializeComponent();
        }

        private void Encode_Click(object sender, RoutedEventArgs e)
        {
            this.Result.Text = IdConvert.ConvertToShortGuidString(this.Orignal.Text);
        }

        private void Decode_Click(object sender, RoutedEventArgs e)
        {
            this.Result.Text = IdConvert.ConvertShortGuidStringToOrignalId(this.Orignal.Text);
        }
    }
}
