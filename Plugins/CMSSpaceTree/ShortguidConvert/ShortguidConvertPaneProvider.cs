﻿using CMSSpaceTree.CMSSpaceTree;
using Lusa.UI.WorkBenchContract.Controls.Pane;

namespace CMSSpaceTree
{
    public class ShortguidConvertPaneProvider : IPaneViewDescriptorProvider
    {
        public ShortguidConvertPaneProvider()
        {
            new SyncLogger("msg");
            new SyncLogger("msgdeamon");
        }
        PaneViewDescriptor IPaneViewDescriptorProvider.Pane
        {
            get
            {
                return new PaneViewDescriptor(typeof(ShortguidConvert.ShortguidConvert)) { Header = "ShortguidConvert", IsDocument = true };
            }
        }
    }
}
