using Confluent.Kafka;
using Confluent.Kafka.Serialization;
using Lusa.UI.Msic.MessageService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WorkspaceEventMocker
{
    /// <summary>
    /// Interaction logic for PaneViewUserControl.xaml
    /// </summary>
    public partial class PaneViewUserControl : UserControl
    {
        private const string KafkaEventServer = "104.40.0.161:9092";
        private const string KafkaEventTopic = "seismic.workspace";
        private Producer<Null, string> kafkaProducer;
        public PaneViewUserControl()
        {
            InitializeComponent();

            var config = new Dictionary<string, object>
                {
                    {
                        "bootstrap.servers", string.Join(",", new string[] { KafkaEventServer })
                    }
                };
            kafkaProducer = new Producer<Null, string>(config, new NullSerializer(), new StringSerializer(Encoding.UTF8));
            var mets = kafkaProducer.GetMetadata(true, "seismic.workspace");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var et = EventType.Text;
            var eid = EventResouceId.Text;
            var uid = UserId.Text;
            ProduceEvent(new WorkspaceEvent() { EventType = et, ContentId = eid, UserId = uid });
        }

        private void ProduceEvent(WorkspaceEvent workspaceEvent)
        {
            var str = JsonConvert.SerializeObject(workspaceEvent);
            kafkaProducer.ProduceAsync(KafkaEventTopic, null, str);
            MessageService.Instance.SendMessage("Workspace event sended");
        }

        private void EventType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }

    public class WorkspaceEvent
    {
        public string EventType { get; set; }

        public string ContentId { get; set; }

        public string UserId { get; set; }
    }
}
