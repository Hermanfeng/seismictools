using Lusa.UI.WorkBenchContract.Controls.Pane;

namespace WorkspaceEventMocker
{
    public class WorkspaceEventMockerPanViewProvider : IPaneViewDescriptorProvider
    {
        PaneViewDescriptor IPaneViewDescriptorProvider.Pane
        {
            get
            {
                return new PaneViewDescriptor(typeof(PaneViewUserControl)) { Header = "WorkspaceEventMocker", IsDocument = true };
            }
        }
    }
}
