﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMSTools.ShotGuidToOriginal;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Seismic.Foreshock.Cosmos;

namespace SyncCMSClear
{
    public class SyncCmsClear
    {
        CosmosClientFactory factory;
        private readonly string serverUrl;
        private readonly string authkey;

        public async static Task Test()
        {
            var serviceUrl = "https://localhost:8081";
            var authKey = "C2y6yDjf5/R+ob0N8A7Cgv30VRDJIWEHLM+4QDU5DE2nQ9nDuVTqobD4b8mGGyPMbIZnqyMsEcaGQy67XIw/Jw==";
            var c = new SyncCmsClear(serviceUrl, authKey);
            //await c.ClearSyncKnowledgeCosmosDb();
            await c.ClearSpaceNodes("dev", "2");
        }

        public SyncCmsClear(string serverUrl, string authkey)
        {
            factory = new CosmosClientFactory(new CosmosClientFactoryOptions());
            this.serverUrl = serverUrl;
            this.authkey = authkey;
        }

        private async Task<string> RecoverSpaceAndRootNode(string tenant, string spaceId, string tenantDbId)
        {
            var spaceData = await GetSpaceData(tenant, tenantDbId, spaceId);
            var rootSpaceNodeId = spaceData?.rootNodeId;
            if (!string.IsNullOrEmpty(rootSpaceNodeId))
            {
                var rootNode = await GetSpaceNode(tenant, tenantDbId, spaceId, rootSpaceNodeId);
                if(rootNode==null)
                {
                    // space is existed, but root node is not existed
                    // remove the space
                    await this.DeleteSpaceData(tenantDbId, spaceData.id, spaceData.partitionKey);
                }
            }
            else
            {
                //no space
                //no space, but root space id existed (dirty data)
            }
            return rootSpaceNodeId;
        }

        private async Task<string> GetTenantDbId(string tenant)
        {
            var platformDbId = "dev_platform_db";

            //var DbId = "dev_geonetwork1_multitenant_shared_replicated_db";
            var DbId = await GetTenantDbId(tenant, platformDbId);
            return DbId;
        }

        public async Task ClearSyncKnowledgeCosmosDb()
        {
            var databaseId = "dev_sync_db";
            var collectionId = "sync_item_knowledge_col";
            var client = await factory.GetCosmosClientAsync(new Uri(serverUrl), authkey, databaseId, collectionId);
            
            var url = UriFactory.CreateDocumentCollectionUri(databaseId, collectionId);
            try
            {
                var r = await client.DocumentClient.ReadDocumentCollectionAsync(url);
                if (r != null)
                {
                    await client.DocumentClient.DeleteDocumentCollectionAsync(r.Resource.SelfLink);
                }
            }
            catch(Exception ex)
            {

            }
            var dburl = UriFactory.CreateDatabaseUri(databaseId);
            var db = await client.DocumentClient.ReadDatabaseAsync(dburl);
            await client.DocumentClient.CreateDocumentCollectionAsync(db.Resource.SelfLink, new DocumentCollection() { Id = collectionId, PartitionKey = new PartitionKeyDefinition() { Paths = new System.Collections.ObjectModel.Collection<string>() { "/partitionKey" } } });
  
        }

        public async Task ClearSpaceNodes(string tenant,string spaceId)
        {
            spaceId = IdConvert.ConvertToShortGuidString(spaceId);


            var DbId = await GetTenantDbId(tenant);

            var rootSpaceNodeId = await RecoverSpaceAndRootNode(tenant, spaceId, DbId);

            var nodes = await GetSpaceNodes(tenant, DbId, spaceId);

            if(nodes!=null)
            {
                foreach(var node in nodes)
                {
                    if (node.nodeId != rootSpaceNodeId)
                    {
                        await DeleteSpaceNode(DbId, node.id, node.partitionKey);
                    }
                }
            }
        }

        private async Task<string> GetRootNodeId(string tenant, string spaceId, string dbId)
        {
            var CollId = "multitenant_cms_metadata_col";
            var sql = $"select * from c where c.partitionKey = '{tenant}' and c.tenantId = '{tenant}' and c.kind = 'Space' and c.spaceId = '{spaceId}'";
            var data = await GetData(sql, tenant, dbId, CollId);
            var rootNodeId = data?.FirstOrDefault()?.rootNodeId;
            return rootNodeId;
        }

        private async Task<dynamic> GetTenantData(string tenant,string platformDbId)
        {
            var CollId = "platform_col";
            var sql = $"select * from c where c.tenantId = '{tenant}' and c.kind = 'Tenant'";
            var data = await GetData(sql, tenant, platformDbId, CollId);
            return data.FirstOrDefault();
        }

        private async Task<dynamic> GetGeoData(string tenant, string dbId, string geoId)
        {
            var CollId = "platform_col";
            var sql = $"select * from c where c.geoNetworkId = '{geoId}' and c.kind = 'GeoNetwork'";
            var data = await GetData(sql, tenant, dbId, CollId);
            return data.FirstOrDefault();
        }

        private string GetDbId(dynamic geoData, string serviceTier)
        {
            if(geoData!=null)
            {
                if(serviceTier == "Shared")
                {
                    return geoData.replicatedDatabaseService.multitenantSharedReplicatedDatabase.database;
                }
                else if(serviceTier == "Standard")
                {
                    return geoData.replicatedDatabaseService.multitenantStandardReplicatedDatabase.database;
                }
                else if (serviceTier == "Premium")
                {
                    return geoData.replicatedDatabaseService.premiumTenantReplicatedDatabase.database;
                }
            }
            return string.Empty;
        }

        private async Task<string> GetTenantDbId(string tenant, string platformDbId)
        {
            var tenantData = await GetTenantData(tenant, platformDbId);
            if(tenantData!=null)
            {
                var geoData = await GetGeoData(tenant, platformDbId, tenantData.defaultGeoNetworkId);
                return GetDbId(geoData, tenantData.serviceTier);
            }
            return string.Empty;
        }

        private async Task<dynamic> GetSpaceNodes(string tenant, string dbId, string spaceId)
        {
            var CollId = "multitenant_cms_node_col";
            var partitionKey = tenant + "_" + spaceId;
            var sql = $"select * from c where c.partitionKey = '{partitionKey}' and c.kind = 'Node' and c.spaceId = '{spaceId}'";
            var data = await GetData(sql, tenant, dbId, CollId);
            return data;
        }

        private async Task<dynamic> GetSpaceNode(string tenant, string dbId, string spaceId, string nodeId)
        {
            var CollId = "multitenant_cms_node_col";
            var partitionKey = tenant + "_" + spaceId;
            var sql = $"select * from c where c.partitionKey = '{partitionKey}' and c.nodeId = '{nodeId}' and c.kind = 'Node' and c.spaceId = '{spaceId}'";
            var data = await GetData(sql, tenant, dbId, CollId);
            return data.FirstOrDefault();
        }

        private async Task<dynamic> GetSpaceData(string tenant, string dbId, string spaceId)
        {
            var CollId = "multitenant_cms_metadata_col";
            var sql = $"select * from c where c.partitionKey = '{tenant}' and c.kind = 'Space' and c.spaceId = '{spaceId}'";
            var data = await GetData(sql, tenant, dbId, CollId);
            return data?.FirstOrDefault();
        }

        private async Task DeleteSpaceData(string dbId, string id, string partitionKey)
        {
            var collId = "multitenant_cms_metadata_col";
            await DeleteDocument(dbId, collId, id, partitionKey);
        }

        private async Task<IEnumerable<dynamic>> GetData(string sql, string tenant, string dbId,string collId)
        {
            var client = await factory.GetCosmosClientAsync(new Uri(serverUrl), authkey, dbId, collId);
            var documents = await client.FindDocumentsAsync<dynamic>(sql, new SqlParameterCollection());
            return documents.Documents;
        }

        private async Task DeleteSpaceNode(string dbId, string id, string partitionKey)
        {
            var collId = "multitenant_cms_node_col";
            await DeleteDocument(dbId, collId, id, partitionKey);
        }



        private async Task DeleteDocument(string dbId, string collId, string id, string partitionKey)
        {
            var client = await factory.GetCosmosClientAsync(new Uri(serverUrl), authkey, dbId, collId);
            await client.DeleteDocumentAsync<dynamic>(id,partitionKey);
        }
    }
}
