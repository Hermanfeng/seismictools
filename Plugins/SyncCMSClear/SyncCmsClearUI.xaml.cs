﻿using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Lusa.UI.WorkBenchContract.Controls.Pane;

namespace SyncCMSClear
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class SyncCmsClearUI : UserControl, IPanViewSerializer
    {
        public SyncCmsClearUI()
        {
            InitializeComponent();
        }

        string IPanViewSerializer.Serializer()
        {
            var splitor = "||||";
            StringBuilder sb = new StringBuilder();
            sb.Append(this.CosmosDbEndpoit.Text);
            sb.Append(splitor);
            sb.Append(this.TenantId.Text);
            sb.Append(splitor);
            sb.Append(this.SpaceId.Text);
            return sb.ToString();
        }

        void IPanViewSerializer.DeSerializer(string data)
        {
            var splitor = "||||";
            var lines = data.Split(new string[] { splitor }, StringSplitOptions.None);
            this.CosmosDbEndpoit.Text = lines[0];
            this.TenantId.Text = lines[1];
            this.SpaceId.Text = lines[2];
        }

        private async  void ClearAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var serviceUrl = this.CosmosDbEndpoit.Text;
                var authKey = CosmosServiceUrlAuthMap.Local;
                var tenantId = this.TenantId.Text;
                var spaceIds = this.SpaceId.Text.Split(new string[] { "," },StringSplitOptions.RemoveEmptyEntries);
                var c = new SyncCmsClear(serviceUrl, authKey);

                await c.ClearSyncKnowledgeCosmosDb();
                foreach (var spaceId in spaceIds)
                {
                    await c.ClearSpaceNodes(tenantId, spaceId);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            MessageBox.Show("All Cleared!");
        }

        private async void ClearKnowledge_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var serviceUrl = this.CosmosDbEndpoit.Text;
                var authKey = CosmosServiceUrlAuthMap.Local;
                var c = new SyncCmsClear(serviceUrl, authKey);

                await c.ClearSyncKnowledgeCosmosDb();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            MessageBox.Show("Knowledge Cleared!");
        }
    }
}
