﻿using Lusa.UI.WorkBenchContract.Controls.Pane;

namespace SyncCMSClear
{
    public class SyncCMSClearPaneProvider : IPaneViewDescriptorProvider
    {
        PaneViewDescriptor IPaneViewDescriptorProvider.Pane
        {
            get
            {
                return new PaneViewDescriptor(typeof(SyncCMSClear.SyncCmsClearUI)) { Header = "SyncCMSClear", IsDocument = true };
            }
        }
    }
}
