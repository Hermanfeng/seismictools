using Lusa.UI.WorkBenchContract.Controls.Pane;

namespace SyncCMSClear
{
    public class SyncCMSClearPanViewProvider : IPaneViewDescriptorProvider
    {
        PaneViewDescriptor IPaneViewDescriptorProvider.Pane
        {
            get
            {
                return new PaneViewDescriptor(typeof(PaneViewUserControl)) { Header = "SyncCMSClear", IsDocument = true };
            }
        }
    }
}
