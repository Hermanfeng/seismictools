﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace CMSTools
{
    public sealed class ShortGuid
    {
        private const int FixedLength = 22;

        private static readonly Regex Pattern = new Regex(@"^[a-zA-Z\d\-_]{22}$");

        private readonly Guid _guid;

        public ShortGuid(Guid guid)
        {
            _guid = guid;
        }

        public static ShortGuid Create()
        {
            return new ShortGuid(Guid.NewGuid());
        }

        public static ShortGuid Parse(string text)
        {
            return new ShortGuid(Decode(text));
        }

        public static bool TryParse(string text, out ShortGuid value)
        {
            if (!TryDecode(text, out Guid guid))
            {
                value = null;
                return false;
            }
            else
            {
                value = new ShortGuid(guid);
                return true;
            }
        }

        public static string Encode(Guid guid)
        {
            var encoded = Convert.ToBase64String(guid.ToByteArray());
            encoded = encoded.Replace('/', '_').Replace('+', '-');
            encoded = encoded.Substring(0, FixedLength);
            return encoded;
        }

        public static Guid Decode(string text)
        {
            if (!TryDecode(text, out Guid guid))
            {
                throw new ArgumentException($"invalid short guid {text}", nameof(text));
            }

            return guid;
        }

        public static bool TryDecode(string text, out Guid guid)
        {
            if (!IsValid(text))
            {
                guid = Guid.Empty;
                return false;
            }
            else
            {
                text = text.Replace('_', '/').Replace('-', '+');
                text = text + "==";
                var bytes = Convert.FromBase64String(text);
                guid = new Guid(bytes);
                return true;
            }
        }

        public static bool IsValid(string text)
        {
            return Pattern.IsMatch(text);
        }

        public override bool Equals(object obj)
        {
            var guid = obj as ShortGuid;
            return guid != null &&
                   _guid.Equals(guid._guid);
        }

        public override int GetHashCode()
        {
            return _guid.GetHashCode();
        }

        public override string ToString()
        {
            return Encode(_guid);
        }

        public Guid ToGuid()
        {
            return _guid;
        }
    }

}
