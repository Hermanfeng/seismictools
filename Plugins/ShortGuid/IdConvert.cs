﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMSTools.ShotGuidToOriginal
{
    public static class IdConvert
    {
        public static string ConvertToShortGuidString(string originalId)
        {
            if (Guid.TryParse(originalId, out var guid))
            {
                return ShortGuid.Encode(guid);
            }

            // int
            byte[] bytes = new byte[16];
            BitConverter.GetBytes(Convert.ToInt32(originalId)).CopyTo(bytes, 0);
            return ShortGuid.Encode(new Guid(bytes));
        }

        public static string ConvertShortGuidStringToOrignalId(string shotguid)
        {
            if (ShortGuid.TryDecode(shotguid, out var guid))
            {
                var str = guid.ToString();
                if(str.EndsWith("-0000-0000-0000-000000000000"))
                {
                    var intvalue = int.Parse(str.Substring(0, 8),System.Globalization.NumberStyles.HexNumber);
                    return intvalue.ToString();
                }
                return str;
            }

            return string.Empty;
        }

        public static string ToShortguid(this string guid)
        {
            return IdConvert.ConvertToShortGuidString(guid);
        }

        public static string ToOrignalId(this string shortguid)
        {
            return IdConvert.ConvertShortGuidStringToOrignalId(shortguid);
        }
    }
}
